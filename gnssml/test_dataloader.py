import numpy as np
from pathlib import Path
from rich import print
import torch
import logging
from datetime import datetime
from dataloaders import GNSSDataloader
from datasets import GNSSDataset
from filelog import create_logging

if __name__=="__main__":
    base = Path(__file__).parent.resolve().parent
    model_name = "test_gnssdataset"
    create_logging(base, model_name)
    base = Path(__file__).parent.resolve()
    dataset = base.parent.parent / "datasets/timeserie"
    dataset_fft = base.parent.parent / "datasets/fft_out"
    groups = base.parent.parent / "datasets/graph_neightboor_stations_set.data"
    dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups, 
        test=True)
    print(dataset.groups)
    print(dataset.indexes)
    print(dataset.re_groups)
    dataloader = GNSSDataloader(
        dataset, 
        batch_size=64)

    for i, (data_x, data_y) in enumerate(dataloader):
        print(i, data_x.size(), data_y.size())
        print("First item",i, data_x[0].size(), data_y[0].size())

        if i == 1:
            break
