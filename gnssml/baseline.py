import logging
import math
import time
from pathlib import Path
from datetime import datetime
from dataclasses import dataclass
from typing import Dict, List
import numpy as np
from rich import print

import torch
import torch.optim as optim
from torch.autograd import Variable
from torch.nn import functional as F
from torch import nn
from torch.utils.data import DataLoader
from datasets import GNSSDataset
from dataloaders import GNSSDataloader
from models.cnn_lineal_baseline import CNNLinealBaseline
from managers import (
    Stage,
    Axis,
    ModelManager,
    OptimizerManager,
    CriterionManager,
    UniversalManager,
    LossAndAcc,
    LossAndAccManager,
    UniversalIterator)
from filelog import create_logging
import psutil

this_directory = Path(__file__).parent.resolve()


def calculate_accuracy(y_pred, y_true):
    """Calculates the accuracy
    Root Mean Squared Error (RMSE)
    """
    return torch.sqrt(F.mse_loss(y_true,y_pred))

#Define a function to perform training
AXES = ["n","e","u"]
TOTAL = {
    Axis.E:0.,
    Axis.U:0.,
    Axis.N:0.
}

def get_memory():
    virtual = psutil.virtual_memory()
    swap = psutil.swap_memory()
    logging.warning(f"""RAM:: Available {virtual.available}. Used {virtual.percent}% RAM. SWAP {swap.used/swap.total} % swap""")
    

def train(
        iterator, 
        universal,
        device):
    #We have to set the neural network in training mode. This is because during
    #training, we need gradients and complementary data to ease the computation  
    universal.train()
    loss_acc = LossAndAccManager.create()

    #Training loop
    for i, selector in enumerate(iterator):
        if i%100==0:
            get_memory()
        for axis in AXES:
            selector.set_status(axis)
            x, y = selector.values()
            #y = y[1,:].squeeze(dim=2)
            logging.info(f"Iteraring on axis {axis}")
            universal.set_status(axis)
            loss_acc.set_status(axis)
            model, optimizer, criterion = universal.objects()
            model.to(device)
            #breakpoint()
            # add total elements
            # loss_acc.add_loss(y)
            # x = x.to(device) #Data x already on device
            # y = y.long().to(device) #Labels already on device
            #coutn length in loss
            loss_acc.add_total(y)

            optimizer.zero_grad() #Clean gradients
            x = x.cuda()
            y = y.cuda()
            y_pred = model(x) #Feed the network with data
            logging.info(f"Y_pred dtype {y_pred.dtype}")
            logging.info(f"y {y.dtype}")

            loss = criterion(y_pred, y) #Compute the loss

            acc = calculate_accuracy(y_pred, y) #Compute the accuracy

            loss.backward() #Compute gradients

            optimizer.step() #Apply update rules

            loss_acc.add_loss(loss.item())
            loss_acc.add_acc(acc.item())
    return loss_acc


#Function to test neural network

def evaluate(
        iterator, 
        universal,
        device):
    #We put the network in testing mode
    #In this mode, Pytorch doesn't use features only reserved for 
    #training (dropout for instance)    
    universal.eval()
    loss_acc = LossAndAccManager.create()

    with torch.no_grad(): #disable the autograd engine (save
        #computation and memory)

      for selector in iterator:
        for axis in AXES:
            selector.set_status(axis)
            x, y = selector.values()
            #y = y[1,:].squeeze(dim=2)
            logging.info(f"Iteraring on axis {axis}")
            universal.set_status(axis)
            loss_acc.set_status(axis)
            model, optimizer, criterion = universal.objects()
            model.to(device)

            #coutn length in loss
            loss_acc.add_total(y)

            y_pred = model(x)

            loss = criterion(y_pred, y)

            acc = calculate_accuracy(y_pred, y)

            loss_acc.add_loss(loss.item())
            loss_acc.add_acc(acc.item())

      # todo obtain total items
      return loss_acc


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs

def do_the_training(
        universal:UniversalManager, 
        train_iterator,
        valid_iterator, 
        epochs:int=10):
    #Let's perform the training
    device = torch.device('cuda' if torch.cuda.is_available() else
                          'cpu')

    best_valid_loss = float('inf')

    for epoch in range(epochs):

        start_time = time.time()

        #Train + validation cycles  

        for i, selector in enumerate(train_iterator):
            if i%100==0:
                get_memory()
            x, y = selector.values()
      
      # loss_acc_train = train(
      #     train_iterator, 
      #     universal, 
      #     device, 
      #     loss_acc)

      # loss_acc_valid = evaluate(
      #     valid_iterator, 
      #     universal,
      #     device, 
      #     loss_acc)


      # valid_loss, valid_acc = loss_acc_valid.elems()

      # #If we find a smaller loss, we save the model
      # if valid_loss < best_valid_loss:
      #   best_valid_loss = valid_loss
      #   universal.save(epoch, best_valid_loss)

          # end_time = time.time()

          # epoch_mins, epoch_secs = epoch_time(start_time, end_time)

      # logging.info(f'Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
      # logging.info(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
      # logging.info(f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%')


def test_do_train(
        universal:UniversalManager, 
        iterator:UniversalIterator, 
        epochs:int=10):
    device = torch.device('cuda' if torch.cuda.is_available() else
                          'cpu')
    best_valid_loss = float('inf')

    universal.train()
    axes = ["n","e","u"]
    total = {
        Axis.E:0.,
        Axis.U:0.,
        Axis.N:0.
    }

    for epoch in range(epochs):

        start_time = time.time()
        # train part
        iterator.set_stage("train")
        train_iterator = iterator.get()
        loss_acc_train = train(
            train_iterator, 
            universal, 
            device)


        iterator.set_stage("validation")
        valid_iterator = iterator.get()
        loss_acc_valid = evaluate(
            valid_iterator, 
            universal,
            device)
        end_time = time.time()

        for axis in axes:
            loss_acc_train.set_status(axis)
            loss_acc_valid.set_status(axis)
            train_loss, train_acc = loss_acc_valid.elems()
            valid_loss, valid_acc = loss_acc_valid.elems()
            #If we find a smaller loss, we save the model
            if valid_loss < best_valid_loss:
              best_valid_loss = valid_loss
              universal.save(epoch, best_valid_loss)
            # measure time
            epoch_mins, epoch_secs = epoch_time(start_time, end_time)

            logging.info(f'Axis {axis},\t Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
            logging.info(f'Axis {axis},\t Train Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
            logging.info(f'Axis {axis},\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%')


def load_dataset_iterators(path:Path, dataset_selection:Dict[str, List[int]]):
    dataset = path / "datasets/timeserie"
    dataset_fft = path / "datasets/fft_out"
    groups = path / "datasets/graph_neightboor_stations_set.data"
    train_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        dataset_selection,
        "train",
        test=True)
    val_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        dataset_selection,
        "validation",
        test=True)
    test_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        dataset_selection,
        "test",
        test=True)

    train_dataloader = GNSSDataloader(
        train_dataset, 
        batch_size=64)
    val_dataloader = GNSSDataloader(
        val_dataset, 
        batch_size=64)
    test_dataloader = GNSSDataloader(
        test_dataset, 
        batch_size=64)

    return UniversalIterator(train_dataloader, val_dataloader, test_dataloader)
            

if __name__=="__main__":
    #nn.CrossEntropyLoss()# clasification
    print(torch.cuda.is_available())
    dataset_selection = {
        Stage.TRAIN: list(range(0,10)),
        Stage.VALIDATION: [10,11],
        Stage.TEST:[12]
    }
    model_name = "cnn_lineal_baseline"
    create_logging(this_directory, model_name, logging.DEBUG)
    learning_rate = 0.001
    momentum = 0.9
    batch_size_train = 100
    batch_size_test = 20
    epochs = 1
    # create the manager for 3 axis
    models_path = this_directory.parent.parent / f"models/{model_name}/"
    models_path.mkdir(parents=True, exist_ok=True)
    logging.info(f"Models path {models_path}")

    channels=1
    model_manager = ModelManager.create(
        CNNLinealBaseline, 
        models_path,
        channels)
    optimizer = OptimizerManager.create(model_manager, learning_rate)
    criterion = CriterionManager.create()

    ### dataset iterators

    universal_iterator = load_dataset_iterators(
        this_directory.parent.parent, 
        dataset_selection)

    # move to cuda
    universal = UniversalManager(
        model_manager, 
        optimizer, 
        criterion)

    print("Try test do train")
    test_do_train(
        universal, 
        universal_iterator, 
        epochs)
