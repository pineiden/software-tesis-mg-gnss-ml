from dataclasses import dataclass
import torch
from gnssml.managers import Axis, Switcher

@dataclass
class DataSlice(Switcher):
    E_fft: torch.FloatTensor
    N_fft: torch.FloatTensor
    U_fft: torch.FloatTensor  
    pairs_distance: torch.FloatTensor
    main_position: torch.FloatTensor

    def x(self)->torch.FloatTensor:
        return self.get()

    def y(self)->torch.FloatTensor:
        match self.status:
            case Axis.N:
                return self.N_fft
            case Axis.U:
                return self.U_fft
            case Axis.E:
                return self.E_fft

    def distances(self)->torch.FloatTensor:
        return self.pairs_distance

    def position(self)->torch.FloatTensor:
        return self.main_position

    def to(self, device):
        """
        Move torch elements to device
        """
        self.E.to(device)
        self.E_fft.to(device)
        self.N.to(device)
        self.N_fft.to(device)
        self.U.to(device)
        self.U_fft.to(device)
        self.pairs_distance.to(device)
        self.main_position.to(device)
