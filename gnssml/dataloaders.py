from dataclasses import dataclass
from typing import Generic, Tuple,TypeVar, Dict
import torch

from gnssml.datasets import GNSSDataset
from gnssml.managers import Iterator, Switcher, Axis

TFloat = TypeVar("TorchFloat")

@dataclass
class Selector(Switcher):
    distances:Dict[Tuple[str,str], float]
    position:torch.FloatTensor

    def values(self):
        return (self.get(), self.distances, self.position)

@dataclass
class GNSSDataloader(Generic[Iterator]):
    """
    This dataloader doesn't need to know the length of the dataset
    """
    dataset: GNSSDataset
    batch_size: int
    run:bool=True
    device: str = 'cuda' if torch.cuda.is_available() else 'cpu'  
# Establecer dispositivo (GPU si está disponible, de lo contrario, CPU)

    def __iter__(self):
        AXES = ["N","E","U"]
        X = []
        Y = []
        counter = 0
        count = False
        item = {axis:{"x":[], "y":[], "distances":[]} for axis in AXES}

        for data in self.dataset:
            distances = data.distances()
            position = data.position()
            datasets = {}
            for axis in AXES:
                data.set_status(axis)
                x = data.x()
                y = data.y()
                # default value 600, hardcoded, check how rescue
                if len(x)==self.dataset.window and len(y)==self.dataset.window:
                    item[axis]["x"].append(x)
                    item[axis]["y"].append(y)
                    count = True

            if count:
                counter += 1
                count = False
            if counter==self.batch_size:
                try:
                    X = item["E"]["x"]
                    Y = item["E"]["y"]
                    batch_x = torch.stack(X).unsqueeze(1)
                    batch_y = torch.stack(Y).unsqueeze(1)
                    datasets["E"] = (batch_x,batch_y.squeeze())
                    X = item["N"]["x"]
                    Y = item["N"]["y"]
                    batch_x = torch.stack(X).unsqueeze(1)
                    batch_y = torch.stack(Y).unsqueeze(1)
                    datasets["N"] = (batch_x,batch_y.squeeze())
                    X = item["U"]["x"]
                    Y = item["U"]["y"]
                    batch_x = torch.stack(X).unsqueeze(1)
                    batch_y = torch.stack(Y).unsqueeze(1)
                    datasets["U"] = (batch_x,batch_y.squeeze())
                    item = {axis:{"x":[], "y":[]} for axis in AXES}
                    counter = 0
                    yield Selector(**datasets, distances=distances, position=position, status=Axis.E)
                except Exception as ex:
                    breakpoint()
                    raise ex

    def set_status(self,axis:str):
        self.dataset.set_status(axis)

    def stop(self):
        self.run = not self.run
        

