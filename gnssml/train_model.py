from gnssml.managers import (
    Axis,
    UniversalManager,
    LossAndMetricsManager)
from gnssml.tools import (
    dt_filename, 
    save_data_loss, 
    calculate_dtw, 
    epoch_time,
    get_memory)
import logging
import torch
import os
#disable benchmark
torch.backends.cudnn.enabled = False
#torch.cuda.set_enabled_lms(True)
os.environ["PYTORCH_CUDA_ALLOC_CONF"] ="max_split_size_mb:512"
os.environ["COMMANDLINE_ARGS"]="--xformers --no-half --opt-sdp-attention --medvram"

def train(
        iterator, 
        universal:UniversalManager,
        device:str, 
        epoch:int,
        loss_metrics:LossAndMetricsManager, 
        softDTW, 
        axes):
    #We have to set the neural network in training mode. This is because during
    #training, we need gradients and complementary data to ease the computation  
    universal.train()

    cpu = torch.device('cpu')

    #Training loop
    for i, selector in enumerate(iterator):
        loss_metrics.new_metrics(epoch)
        if i%100==0:
            get_memory()
            print(f"Allocated GPU Mem", torch.cuda.memory_allocated())

        #for axis in Axis:
        for axis in axes:
            loss_metrics.set_status(axis)
            selector.set_status(axis)
            (x, y), distances, position = selector.values()
            #y = y[1,:].squeeze(dim=2)
            logging.info(f"Iteraring on axis {axis}")
            universal.set_status(axis)
            softDTW.set_status(axis)
            model, optimizer, criterion = universal.objects()
            model.to(device)
            model.set_distances(distances)
            model.set_position(position)
            optimizer.zero_grad() #Clean gradients
            x = x.cuda()
            y = y.cuda()
            y_pred = model(x) #Feed the network with data
            logging.info(f"Y_pred dtype {y_pred.dtype}")
            logging.info(f"y {y.dtype}")

            loss = criterion(y_pred, y) #Compute the loss
            dtw = calculate_dtw(softDTW, y_pred, y) #Compute the accuracy
            #loss.backward() #Compute gradients

            dtw.backward()

            #model.to(cpu)
            optimizer.step() #Apply update rules

            # fille the loss and metrics
            loss_metrics.add_dtw(dtw.item())
            loss_metrics.add_loss(loss.item())
            model.to(cpu)

