from pathlib import Path
import ujson as json
import random
import torch
from torch.utils.data import Dataset
from torchvision import datasets
from torchvision.transforms import ToTensor
import numpy as np
import copy
import pickle
from rich import print
from typing import List, Dict, Tuple, Any
import clases
import pandas as pd
from dataclasses import dataclass
from dataslice import DataSlice
from rich import print


def read_path(path:Path):
    databytes = path.read_bytes()
    dataset = pickle.loads(databytes)
    return dataset



def buffer_sets(groups:Dict[str, List[str]]):
    """
    define the sets where every station is used.

    Once used on use item, discart it until all is empty, then
    clear the file loaded.

    """
    buffers = {}

    for name, group in groups.items():
        codes = set([name] + group)
        for code in codes:
            for name,group in groups.items():
                codes_in = set([name] + group)
                if code in codes_in: 
                    if code not in buffers:
                        buffers[code] = {name}
                    else:
                        buffers[code].add(name)
    return buffers


def join_fft(fft):
    axis_e = np.concatenate([np.array([0.])]+list(fft["E"].values()))
    axis_n = np.concatenate([np.array([0.])]+list(fft["N"].values()))
    axis_u = np.concatenate([np.array([0.])]+list(fft["U"].values()))

    return axis_e, axis_n, axis_u

from enum import IntEnum

class Layers(IntEnum):
    Separated = 0
    Together  = 1

class GNSSDataset(Dataset):
    def __init__(
            self, 
            dataset:Path, 
            dataset_fft:Path,
            groups:Path, 
            window=600, 
            step:int=5,
            selection:List[str]=["first","second","random"],
            test=False, 
            channels:Layers=Layers.Separated,
            *args,
            **kwargs):
        super().__init__(*args,**kwargs)
        self.dataset = dataset
        self.test = test
        self.window = window
        self.step = step
        self.dataset_fft = dataset_fft
        self.buffers_datasets = {}
        self.selection = selection
        # post init values 
        self.data_paths = self.paths()
        self.groups = read_path(groups)
        self.indexes = self.indexes_sets()
        self.re_groups = self.rearrange()
        self.channels = channels

    def read_input(self, code):
        filepath = self.dataset / code
        return read_path(filepath)

    def read_output(self, code):
        filepath = self.dataset_fft / f"{code}.data"
        return read_path(filepath)


    def indexes_sets(self):
        """
        define the sets where every station is used.
        
        Once used on use item, discart it until all is empty, then
        clear the file loaded.

        """
        order = []
        indexes = {}
        codes = set()
        for name, groups in self.groups.items():
            if not order:
                order = list(groups.keys())
            codes |= set(groups.keys())
            indexes[name] = buffer_sets(groups)


        keys = self.groups.keys()
        # make the join
        last_indexes = {}
        for code in codes:
            last_indexes[code] = set()
            for key in keys:
                last_indexes[code] |= indexes[key][code]

        return {code:last_indexes[code] for code in order}

    def paths(self):
        data_paths = {}

        for path in self.dataset.rglob("*.data"):
            code = path.parent.name
            if code not in data_paths:
                data_paths[code] = []
            filename_nro = int(path.name.replace(".data",""))
            filename = path.name
            data_paths[code].append((filename_nro, path))

        for values in data_paths.values():
            values.sort(key=lambda x:x[0])

        return data_paths


    def rearrange(self):
        """
        Extract all keys, suposed all groups have same keys
        """
        code = self.selection[0]
        codes = list(self.groups[code].keys()) 
        groups = {}
        # every item on selection must be in groups
        for code in codes:
            item = {s:self.groups[s][code] for s in self.selection if
                    s in self.groups}
            groups[code] = item
        # this order allow a more efficient use of the datasets
        return groups

    def build_data_matrix(self, maincode, dataset, fft):
        """
        dataset : dict ordered with codes and datasets by code

        fft: must be a numpy array
        """
        field = "timestamp"
        dataframes = {}
        for code, values in dataset.items():
            #breakpoint()
            station = pd.DataFrame([data.usar for data in values["timeserie"]])
            dataframes[code] = station

        axes = ["E", "N", "U"]
        dataframes_axis = {}
        ts ="timestamp"
        # parse to dataframe to be easy fill gaps
        # here build the matrix using first df merge

        df_fft = pd.DataFrame(columns=[ts] + [f"{axis}_fft" for
                                       axis in axes])
        df_fft[ts] = fft[ts]
        df_fft.loc[:,ts] = df_fft.timestamp.apply(int)

        df_fft["E_fft"], df_fft["N_fft"], df_fft["U_fft"] = join_fft(fft)       



        for axis in axes:
            # main station join with its fft signal
            df = dataframes[maincode][[ts, axis]]
            df.loc[:,ts] = df.timestamp.apply(int)
            df = df.rename(columns={axis:f"{axis}_{maincode}"})
            # join with fft         

            df = pd.merge(
                df_fft[[ts, f"{axis}_fft"]], 
                df, 
                on="timestamp", 
                how="inner")

            for code in dataset.keys():
                if code != maincode:
                    dfcode = dataframes[code][[ts, axis]]
                    dfcode = dfcode.rename(columns={axis:f"{axis}_{code}"})
                    dfcode.loc[:,ts] = dfcode.timestamp.apply(int)

                    # merge with main dataframe
                    df = pd.merge(df,dfcode, on="timestamp", how="left")
            df = df.fillna(0)
            df.sort_values(by=[ts])
            columns = ["timestamp", f"{axis}_fft"]+[f"{axis}_{c}" for c in dataset.keys()]
            dataframes_axis[axis] = df.reindex(columns=columns)    
        
        #breakpoint()y
        return dataframes_axis

    def matrix_to_torch(self, matrix_layers, field):
        keys = sorted(matrix_layers.keys())
        key = keys[0]
        fft = f"{field}_fft"
        # fft matrix for this field to -> numpy array
        MM_fft = matrix_layers[key][field][[fft]].to_numpy(dtype=np.float32)
        
        matrixes = []
        for group, matrix in matrix_layers.items():
            df = matrix[field].drop(labels=["timestamp", fft], axis=1)
            np_matrix = df.to_numpy(dtype=np.float32)
            matrixes.append(np_matrix)

        np_matrix_Nd = np.stack(matrixes, axis=2)
        return torch.from_numpy(np_matrix_Nd), torch.from_numpy(MM_fft)

    def __iter__(self)->DataSlice:
        # re arrange groups to be more efficient
        groups = self.re_groups
        # a copy in case of reset, the elements will be deleted
        indexes = copy.deepcopy(self.indexes)
        dataset_buffers = {}
        # name is the group of dataset: first, second, random, etc
        for main_code, group in groups.items():
            # group = dict { name1: neigh1, name2: neigh2,... }
            # code of the stations an its group
            
            # lists to accumulate matrix
            matrix_layers = {}
            first = True
            big_group_codes = set()
            for name, neighboors in group.items():
                # rescue the group of stations where the code exists
                # and could be used
                n = int(len(neighboors)  / 2)
                #this order is to manage the convolution with center
                #on 'main_code' station
                group_codes = neighboors[0:n] + [main_code] + neighboors[n::]
                # read every station of this group and save on the
                # buffer:
                # 1) read the base data timeserie
                # 2) read the fft out dataset
                group_dataset = {}
                # read from files or load from group_dataset
                if self.test:
                    print("Test workin on", group_codes)
                for code in group_codes:
                    # generate structure {"fft": Dataset,
                    # "timeserie":List[Path]}
                    if code not in dataset_buffers:
                        dataset = {"timeserie":[]}
                        dataset["fft"] = self.read_output(code)
                        # get from buffer
                        # data_paths = self.data_paths[code]
                        dataset["timeserie"] = np.concatenate([read_path(path) for
                                                index,path in
                                                self.data_paths[code]])
                        # new big dataset to manage the paths inside the group
                        group_dataset[code] = dataset
                        dataset_buffers[code] = dataset
                    else:
                        # main_code or code?
                        # if was readed...
                        group_dataset[code] = dataset_buffers[code]
                # iterate over group_dataset
                # and read the files
                item = group_dataset[main_code]
                fft = item["fft"]

                data_matrix = self.build_data_matrix(
                    main_code, 
                    group_dataset, 
                    fft)
                big_group_codes |= set(group_codes)
                matrix_layers[name] = data_matrix
            # matrix by axis to torch
            MM_E,MM_fft_E = self.matrix_to_torch(matrix_layers, "E")
            MM_N,MM_fft_N = self.matrix_to_torch(matrix_layers, "N")
            MM_U,MM_fft_U = self.matrix_to_torch(matrix_layers, "U")

                
            window = 0
            # to use for 
            len_data = len(MM_E)
            if self.window > len_data or self.window <=1:
                window = len_data
            else:
                window = self.window
            count = 0
            while count <= len_data -  window:
                win_data = {
                    "E":MM_E[count:count+window,:],
                    "E_fft":MM_fft_E[count:count+window,:],
                    "N":MM_N[count:count+window,:],
                    "N_fft":MM_fft_N[count:count+window,:],
                    "U":MM_U[count:count+window,:],
                    "U_fft":MM_fft_U[count:count+window,:],
                }
                # compose array
                data = DataSlice(**win_data)
                # compose array end
                yield data
                count += self.step
                ############ must check
            # step where the dataset_buffer is cleared if all the
            # uses for the code is complete
            # for every code on the neightboour group, get from
            # buffers and remove main_code from the list
            # on empty list -> delete from buffers
            # CHECK AND TEST THIS:
            for code in big_group_codes:
                code_buffer_group = dataset_buffers.get(code, [])
                if main_code in code_buffer_group:
                    code_buffer_group.remove(main_code)
                if code_buffer_group == []:
                    del dataset_buffers[code]



if __name__=="__main__":
    base = Path(__file__).parent.parent.resolve()
    print("Base" , base)
    dataset = base.parent / "datasets/timeserie"
    dataset_fft = base.parent / "datasets/fft_out"
    groups = base.parent / "datasets/graph_neightboor_stations_set.data"
    dataset = GNSSDataset(dataset, dataset_fft, groups, test=True)
    print(dataset.groups)
    print(dataset.indexes)
    print(dataset.re_groups)
    #dataloader.paths()
    #print(dataloader.data_paths)
    STOP = 10
    for i, data in enumerate(dataset):
        # data is a DatSlice object
        print(f"Iteracion {i}",data.x().shape, data.y().shape)
        data.set_status("n")
        print(f"Iteracion {i}",data.x().shape, data.y().shape)
        data.set_status("u")
        print(f"Iteracion {i}",data.x().shape, data.y().shape)

        if i == STOP:
            break


