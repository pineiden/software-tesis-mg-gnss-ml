from dataclasses import dataclass
import torch.optim as optim
from .model import ModelManager
from .model_types import Axis
from .switcher import Switcher

@dataclass
class OptimizerManager(Switcher):
    
    @classmethod
    def create(cls, 
               model_manager:ModelManager,
               learning_rate:float, 
               *args, 
               **kwargs):
        model_manager.set_status("N")
        model = model_manager.get()
        N = optim.Adam(
            model.parameters(), 
            lr=learning_rate)
        model_manager.set_status("E")
        model = model_manager.get()
        E = optim.Adam(
            model.parameters(), 
            lr=learning_rate)
        model_manager.set_status("U")
        model = model_manager.get()
        U = optim.Adam(
            model.parameters(), 
            lr=learning_rate)
        return OptimizerManager(N=N,E=E,U=U, status=Axis.E)
    

    def get_opt(self, axis:Axis)->optim.Adam:
        match axis:
            case Axis.N:
                return self.N
            case Axis.U:
                return self.U
            case Axis.E:
                return self.E

