from dataclasses import dataclass

from .model import ModelManager
from .optimizer import OptimizerManager
from .criterion import CriterionManager
from .model_types import Axis

@dataclass
class UniversalManager:
    model: ModelManager
    optimizer: OptimizerManager
    criterion: CriterionManager

    def set_status(self, x):
        self.model.set_status(x)
        self.optimizer.set_status(x)
        self.criterion.set_status(x)

    def objects(self):
        return (
            self.model.get(), 
            self.optimizer.get(),
            self.criterion.get()
        )


    def save(self, epoch:int, loss:float, now:str, axis:Axis):
        self.set_status(axis)
        optimizer = self.optimizer.get()
        self.model.save(axis, optimizer, epoch, loss, now)
            
    def eval(self):
        self.model.eval()

    def train(self):
        self.model.train()
