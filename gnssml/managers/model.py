from dataclasses import dataclass
from typing import TypeVar
from pathlib import Path
from torch import nn
from .model_types import Axis, Model
from .switcher import Switcher
import torch

@dataclass
class ModelManager(Switcher):
    path: Path

    def train(self):
        self.N.train()
        self.E.train()
        self.U.train()


    def save(self, axis:Axis, optimizer, epoch:int,  loss:float, now:str):
        data = {
            "epoch":epoch,
            "optimizer_state_dict": optimizer.state_dict(),
            "loss":loss,
            "now": now
        }
        path = self.path / now
        path.mkdir(exist_ok=True)
        match axis:
            case Axis.N:
                data["model_state_dict"] = self.N.state_dict()
                torch.save(data, path / f"N_{epoch}.pt")
            case Axis.U:
                data["model_state_dict"] = self.U.state_dict()
                torch.save(data, path  /f"U_{epoch}.pt")
            case Axis.E:
                data["model_state_dict"] = self.E.state_dict()
                torch.save(data, path  /f"E_{epoch}.pt")


    def load(self, axis:Axis, optimizer, epoch, now):
        path = self.path / now
        match axis:
            case Axis.N:
                checkpoint = torch.load(path /f"N_{epoch}.pt")
                self.N.load_state_dict(checkpoint["model_state_dict"])
                optimizer.N.load_state_dict(checkpoint["optimizer_state_dict"])
                return checkpoint["epoch"], checkpoint["loss"]
            case Axis.U:
                checkpoint = torch.load(path /f"U_{epoch}.pt")
                self.U.load_state_dict(checkpoint["model_state_dict"])
                optimizer.U.load_state_dict(checkpoint["optimizer_state_dict"])
                return checkpoint["epoch"], checkpoint["loss"]
            case Axis.E:
                checkpoint = torch.load(path /f"E_{epoch}.pt")
                self.E.load_state_dict(checkpoint["model_state_dict"])
                optimizer.E.load_state_dict(checkpoint["optimizer_state_dict"])
                return checkpoint["epoch"], checkpoint["loss"]
                
    def eval(self):
        self.E.eval()
        self.N.eval()
        self.U.eval()

    def train(self):
        self.E.train()
        self.N.train()
        self.U.train()

    def to(self,device):
        self.E.to(device)
        self.N.to(device)
        self.U.to(device)

    @classmethod
    def create(cls, class_object, path, *args, **kwargs):
        E = class_object(*args, **kwargs)
        N = class_object(*args, **kwargs)
        U = class_object(*args, **kwargs)
        return ModelManager(N=N, E=E, U=U, status=Axis.E, path=path)


    def main_parameters(self):
        return {
            "e":self.E.main_parameters(), 
            "n":self.N.main_parameters(), 
            "u":self.U.main_parameters()}
