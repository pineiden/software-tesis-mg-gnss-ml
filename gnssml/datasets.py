from pathlib import Path
import ujson as json
import random
import torch
from torch.utils.data import Dataset
#from torchvision import datasets
#from torchvision.transforms import ToTensor
import numpy as np
import copy
import pickle
from rich import print
from typing import List, Dict, Tuple, Any
import gnssml.clases as clases
import pandas as pd
from dataclasses import dataclass
import logging
from datetime import datetime, timedelta
from collections import Counter
from functools import reduce
from gnssml.dataslice import DataSlice
from gnssml.managers import Stage, Axis

def read_path(path:Path):
    databytes = path.read_bytes()
    dataset = pickle.loads(databytes)
    return dataset


def count_frequency(codes:List[str]):
    counter = Counter(codes)
    return counter

def buffer_sets(groups:Dict[str, List[str]]):
    """
    define the sets where every station is used.

    Once used on use item, discard it until all is empty, then
    clear the file loaded.

    """
    buffers = {}

    #iterate over selection
    for name, group in groups.items():
        codes = set([name] + group)
        for code in codes:
            # iterate again over selection
            for name,group in groups.items():
                codes_in = set([name] + group)
                if code in codes_in: 
                    if code not in buffers:
                        buffers[code] = {name}
                    else:
                        buffers[code].add(name)
    return buffers


def join_fft(df_fft, fft):
    try:
        axis_e = fft["E"].values()
        axis_n = fft["N"].values()
        axis_u = fft["U"].values()
        df = pd.DataFrame(columns=["E_fft","N_fft","U_fft"])
        df["E_fft"] = np.concatenate([a for a in axis_e])
        df["N_fft"] = np.concatenate([a for a in axis_n])
        df["U_fft"] = np.concatenate([a for a in axis_u])
        final =  pd.merge(
            df_fft, 
            df, 
            left_index=True,
            right_index=True)
        #breakpoint()
        return final
    except Exception as e:
        breakpoint()
        raise e

def reverse_stages(dataset_selection):
    """
    Values must be hashable and all different
    """
    reverse = {}
    for stage, values in dataset_selection.items():
        for v in values:
            reverse[v] = stage
    return reverse

class GNSSDataset(Dataset):
    def __init__(
            self, 
            dataset:Path, 
            dataset_fft:Path,
            groups:Path,
            pairs_distance:Dict[Tuple[str,str], float],
            dataset_selection:Dict[str, List[int]],
            positions:Dict[str,List[int]],
            stage:str,
            window:int, 
            step:int=5,
            selection:List[str]=["first","second","random"],
            test:bool=False, 
            *args,
            **kwargs):
        super().__init__(*args,**kwargs)
        self.dataset = dataset
        self.test = test
        self.window = window
        self.step = step
        self.dataset_fft = dataset_fft
        self.buffers_datasets = {}
        self.selection = selection
        self.pairs_distance = pairs_distance
        self.main_positions = positions
        # post init values 
        self.reverse = reverse_stages(dataset_selection)
        self.dataset_selection = dataset_selection
        self.data_paths = self.paths()
        self.set_stage(stage)
        self.groups = {k:v for k,v in read_path(groups).items() if k
                       in selection}
        self.indexes = self.indexes_sets()
        self.re_groups = self.rearrange()


    def set_stage(self, name):
        name = name.lower()
        match name:
            case "train":
                self.stage = Stage.TRAIN
            case "validation":
                self.stage = Stage.VALIDATION
            case "test":
                self.stage = Stage.TEST
            case _:
                self.stage = Stage.TRAIN
                

    def read_input(self, code):
        filepath = self.dataset / code
        return read_path(filepath)

    def read_output(self, code):
        filepath = self.dataset_fft / f"{code}.data"
        logging.info(f"Reading {filepath}")
        return read_path(filepath)


    def indexes_sets(self):
        """
        define the sets where every station is used.
        
        Once used on use item, discart it until all is empty, then
        clear the file loaded.


        Create a counter +1 every time a code is used

        self.groups = {
        mode1:{codes},
        mode2:{codes},
        mode3:{codes}
        }
        """
        order = []
        indexes = {}
        codes = set()
        
        # rescue the codes uses on every mode that must be used on the
        # process, using groups
        total = []
        for name, groups in self.groups.items():
            total += reduce(lambda a,b:a+b, [[k]+v for k,v in groups.items()])
            # for code, codes in groups.items():
            #     if "UDAT" in codes:
            #         print(code,"->", codes)

        indexes = count_frequency(total)

        #breakpoint()
        # keys = self.groups.keys()
        # # make the join
        # last_indexes = {}
        # for code in codes:
        #     last_indexes[code] = set()
        #     for key in keys:
        #         last_indexes[code] |= indexes[key][code]
        # {code:last_indexes[code] for code in order}
        return indexes

    def paths(self):
        data_paths = {s:{}  for s in Stage}

        for path in self.dataset.rglob("*.data"):
            code = path.parent.name
            filename_nro = int(path.name.replace(".data",""))
            #breakpoint()
            stage = self.reverse.get(filename_nro, Stage.TRAIN)
            if code not in data_paths[stage]:
                data_paths[stage][code] = []
            filename = path.name
            data_paths[stage][code].append((filename_nro, path))

        # sorting to take ordering data
        for s, groups in data_paths.items():
            for values in groups.values():
                values.sort(key=lambda x:x[0])

        return data_paths


    def rearrange(self):
        code = self.selection[0]
        codes = list(self.groups[code].keys()) 
        groups = {}
        # every item on selection must be in groups
        for code in codes:
            item = {s:self.groups[s][code] for s in self.selection if
                    s in self.groups}
            groups[code] = item
        # this order allow a more efficient use of the datasets
        return groups

    def build_data_matrix(self, maincode, dataset, fft):
        """
        dataset : dict ordered with codes and datasets by code

        fft: must be a numpy array, fft has all data output

        On the merge process cuts to intersection
        """

        ts = "timestamp"
        ts_daily = "ts_daily"
        df_fft = pd.DataFrame(columns=[ts])
        try:
            dataframes = {}
            for code, values in dataset.items():
                #breakpoint()
                station = pd.DataFrame([data.usar for data in values["timeserie"]])
                dataframes[code] = station

            axes = ["E", "N", "U"]
            dataframes_axis = {}
            #ts ="timestamp"
            # parse to dataframe to be easy fill gaps
            # here build the matrix using first df merge

            df_fft[ts] = fft[ts]
            df_fft.loc[:,ts] = df_fft.timestamp.apply(int)

            df_fft = join_fft(df_fft, fft)       
        except Exception as e:
            print("Exception create matrix", maincode, e)
            raise e


        for axis in axes:
            # main station join with its fft signal
            df = dataframes[maincode][[ts, ts_daily, axis]]
            df.loc[:,ts] = df.timestamp.apply(int)
            df.loc[:,ts_daily] = df.ts_daily.apply(int)

            df = df.rename(columns={axis:f"{axis}_{maincode}"})
            # join with fft         
            # match with all fft timeserie and cut interseccion
            df = pd.merge(
                df_fft[[ts, f"{axis}_fft"]], 
                df, 
                on="timestamp", 
                how="inner")

            for code in dataset.keys():
                if code != maincode:
                    dfcode = dataframes[code][[ts, axis]]
                    dfcode = dfcode.rename(columns={axis:f"{axis}_{code}"})
                    dfcode.loc[:,ts] = dfcode.timestamp.apply(int)

                    # merge with main dataframe
                    df = pd.merge(df,dfcode, on="timestamp", how="left")
            df = df.fillna(0)
            df.sort_values(by=[ts])
            columns = ["timestamp", f"ts_daily", f"{axis}_fft"]+[f"{axis}_{c}" for c in dataset.keys()]
            dataframes_axis[axis] = df.reindex(columns=columns)    
        
        #breakpoint()y
        return dataframes_axis

    def matrix_to_torch(self, matrix, field):
        fft = f"{field}_fft"
        MM_fft = matrix[field][[fft]].to_numpy(dtype=np.float32)
        df = matrix[field].drop(labels=["timestamp", fft], axis=1)
        #df = matrix[field].drop(labels=[fft], axis=1)
        np_matrix = df.to_numpy(dtype=np.float32)
        return torch.from_numpy(np_matrix), torch.from_numpy(MM_fft)

    def __iter__(self)->DataSlice:
        # re arrange groups to be more efficient
        groups = self.re_groups
        # a copy in case of reset, the elements will be deleted
        dataset_buffers = {}
        groups_buffers = {k:v for k, v in self.indexes.items()}
        #copy_groups_buffers = copy.deepcopy(groups_buffers)
        #deleted_group = {c:set() for c in self.indexes.keys()}

        print(groups_buffers)

        # name is the group of dataset: first, second, random, etc
        for main_code, group in groups.items():
            # group = dict { name1: neigh1, name2: neigh2,... }
            # code of the stations an its group
            for name, neighboors in group.items():
                # rescue the group of stations where the code exists
                # and could be used
                n = int(len(neighboors)  / 2)
                #this order is to manage the convolution with center
                #on 'main_code' station
                group_codes = neighboors[0:n] + [main_code] + neighboors[n::]

                pairs_distance = torch.from_numpy(
                    np.array([self.pairs_distance[(main_code,code)]
                              for code in group_codes]))
                
                main_position = torch.from_numpy(
                    np.array(self.main_positions.get(main_code)))
                # read every station of this group and save on the
                # buffer:
                # 1) read the base data timeserie
                # 2) read the fft out dataset
                group_dataset = {}
                # read from files or load from group_dataset
                if self.test:
                    print("Test workin on", group_codes)
                for code in group_codes:
                    # generate structure {"fft": Dataset,
                    # "timeserie":List[Path]}
                    if code not in dataset_buffers:
                        start = datetime.utcnow()
                        logging.info(f"READING START:: Reading datasets and fft for {code} - {start}")
                        dataset = {"timeserie":[]}
                        dataset["fft"] = self.read_output(code) if self.test else f"[test] fft for {code}"
                        # get from buffer
                        # data_paths = self.data_paths[code]
                        dataset["timeserie"] = np.concatenate([read_path(path) for
                                                index,path in
                                                self.data_paths[self.stage][code]])
                        # new big dataset to manage the paths inside the group
                        group_dataset[code] = dataset
                        # set the group when the code is inside
                        # make a copy
                        dataset_buffers[code] = dataset
                        end = datetime.utcnow()
                        total = (end-start).total_seconds()
                        logging.info(f"READING END:: Reading datasets and fft for {code} - {end}, {total} secs")

                    else:
                        # main_code or code?
                        # if was readed...
                        group_dataset[code] = dataset_buffers[code]
                # iterate over group_dataset
                # and read the files

                item = group_dataset[main_code]
                fft = item["fft"]
                data_matrix = self.build_data_matrix(
                    main_code, 
                    group_dataset, 
                    fft)
                # matrix by axis to torch
                MM_E,MM_fft_E = self.matrix_to_torch(data_matrix, "E")
                MM_N,MM_fft_N = self.matrix_to_torch(data_matrix, "N")
                MM_U,MM_fft_U = self.matrix_to_torch(data_matrix, "U")

                
                window = 0
                # to use for 
                len_data = len(MM_E)
                if self.window > len_data or self.window <=1:
                    window = len_data
                else:
                    window = self.window
                count = 0
                while count <= len_data -  window:
                    win_data = {
                        "E":MM_E[count:count+window,:],
                        "E_fft":MM_fft_E[count:count+window,:],
                        "N":MM_N[count:count+window,:],
                        "N_fft":MM_fft_N[count:count+window,:],
                        "U":MM_U[count:count+window,:],
                        "U_fft":MM_fft_U[count:count+window,:],
                        "pairs_distance": pairs_distance,
                        "main_position": main_position
                    }
                    # compose array
                    data = DataSlice(**win_data, status=Axis.E)
                    # compose array end
                    yield data
                    count += self.step
                    ############ must check
                # step where the dataset_buffer is cleared if all the
                # uses for the code is complete
                # for every code on the neightboour group, get from
                # buffers and remove main_code from the list
                # case :: on empty list -> delete from buffers
                # CHECK AND TEST THIS:
                # iterating over all codes where MAIN_CODE is used
                for code in group_codes:
                    # drop one for group_buffers
                    logging.warning(f"FREE:: A:: Free {code} on {group_codes}, count: {groups_buffers[code]}")

                    groups_buffers[code] -= 1
                    # then check the counter                   
                    # if empty go inside this if
                    if groups_buffers[code]==0 and code in dataset_buffers:
                        # all task where main_code is used happened
                        # drop from datasets the data associated to code
                        logging.warning(f"FREE:: F:: Droping all group code {code}")
                        #breakpoint()
                        print(f"FREE:: G:: Deleting on code {code}, counter==0 in dataset_buffers", main_code in dataset_buffers)
                        del dataset_buffers[code]
                        del groups_buffers[code]



if __name__=="__main__":
    base = Path(__file__).parent.resolve()
    dataset = base.parent / "datasets/timeserie"
    dataset_fft = base.parent / "datasets/fft_out"
    groups = base.parent / "datasets/graph_neightboor_stations_set.data"
    dataset = GNSSDataset(dataset, dataset_fft, groups, test=True)
    print(dataset.groups)
    print(dataset.indexes)
    print(dataset.re_groups)
    #dataloader.paths()
    #print(dataloader.data_paths)
    STOP = 10
    for i, data in enumerate(dataset):
        # data is a DatSlice object
        print(f"Iteracion {i}",data.x().shape, data.y().shape)
        data.set_status("n")
        print(f"Iteracion {i}",data.x().shape, data.y().shape)
        data.set_status("u")
        print(f"Iteracion {i}",data.x().shape, data.y().shape)

        if i == STOP:
            break


