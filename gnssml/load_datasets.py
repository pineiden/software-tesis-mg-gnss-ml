from typing import Dict, List, Any
from pathlib import Path
from gnssml.datasets import GNSSDataset
from gnssml.dataloaders import GNSSDataloader
from gnssml.managers import UniversalIterator
import pickle
import json

def read_path(path:Path):
    databytes = path.read_bytes()
    dataset = pickle.loads(databytes)
    return dataset

def read_json(path:Path):
    databytes = path.read_text()
    dataset = json.loads(databytes)
    return dataset

def get_positions(metadata):
    item = metadata["position"]["llh"]
    return [item["lon"], item["lat"], item["z"]]

def load_dataset_iterators(path:Path, 
                           dataset_selection:Dict[str,List[int]],
                           batch_size, 
                           window):
    dataset = path /"timeserie"
    dataset_fft = path / "fft_out"
    groups = path / "graph_neightboor_stations_set.data"
    # read this and share in all GNNSdataset
    pairs_distance = read_path(path / "pairs_distance.data")
    positions = {item["code"]:get_positions(item) for item in  read_json(path/"metadata.json")}


    train_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        stage="train",
        window=window,
        test=True)
    val_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        stage="validation",
        window=window,
        test=True)
    test_dataset = GNSSDataset(
        dataset, 
        dataset_fft, 
        groups,
        pairs_distance,
        dataset_selection,
        positions,
        stage="test",
        window=window,
        test=True)

    train_dataloader = GNSSDataloader(
        train_dataset, 
        batch_size=batch_size)
    val_dataloader = GNSSDataloader(
        val_dataset, 
        batch_size=batch_size)
    test_dataloader = GNSSDataloader(
        test_dataset, 
        batch_size=batch_size)

    return dataset, UniversalIterator(train_dataloader, val_dataloader, test_dataloader)
