import httpx

hostname = "165.227.193.0"

def new_machine(register_params):
    url = f"http://{hostname}:8000/new_machine"
    result = httpx.post(url, json=register_params)
    assert result.status_code == 200
    machine = result.json()
    return machine


def get_machine(machine_id):
    url = f"http://{hostname}:8000/get_machine"
    request = httpx.get(url, params={"machine_id":machine_id})
    assert request.status_code == 200
    result = request.json()
    return result
    

def last_register(machine_id):
    url = f"http://{hostname}:8000/last_register"
    start_stage_request = httpx.get(url, params={"machine_id":machine_id})
    assert start_stage_request.status_code == 200
    result_register = start_stage_request.json()
    return result_register
