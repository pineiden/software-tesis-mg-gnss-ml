from inspect import signature
from dataclasses import dataclass

from gnssml.managers import Switcher, Axis

print("Elems", signature(Switcher), signature(Axis))

@dataclass
class SwitcherExample(Switcher):
    def show(self):
        self.set_status(Axis.E)
        print(self.get())
        self.set_status(Axis.N)
        print(self.get())
        self.set_status(Axis.U)
        print(self.get())


def test_switcher_inheritance():
    example = SwitcherExample(E="E",N="N",U="U", status=Axis.E)
    assert example.get()=="E"
    example.set_status("n")
    assert example.get()=="N"
    example.set_status("u")
    assert example.get()=="U"
