import logging
import math
import time
from pathlib import Path
from datetime import datetime
from dataclasses import dataclass
from typing import Dict, List, Any
import numpy as np
from rich import print

import torch
import torch.optim as optim
from torch.autograd import Variable
from torch.nn import functional as F
from torch import nn
from torch.utils.data import DataLoader
from gnssml.datasets import GNSSDataset
from gnssml.dataloaders import GNSSDataloader
from gnssml.models.cnn_lineal_baseline import CNNLinealBaseline
from gnssml.managers import (
    Stage,
    Axis,
    Switcher,
    ModelManager,
    OptimizerManager,
    CriterionManager,
    UniversalManager,
    LossAndMetrics,
    LossAndMetricsManager,
    UniversalIterator)
from gnssml.filelog import create_logging
from gnssml.getsize import getFolderSize
from gnssml.model_flow import new_machine, last_register, get_machine
from gnssml.tools import (
    dt_filename, 
    save_data_loss, 
    calculate_dtw, 
    epoch_time,
    get_memory)
from gnssml.dtwl import DTW
from gnssml.load_datasets import load_dataset_iterators
from gnssml.train_model import train
from gnssml.evaluate_model import evaluate

import httpx
import numpy as np
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import psutil
from dataclasses import dataclass
from datetime import datetime
import pickle


this_directory = Path(__file__).parent.resolve()

softDTW = DTW.create()

#Define a function to perform training
TOTAL = {
    Axis.E:0.,
    Axis.U:0.,
    Axis.N:0.
}

#Function to test neural network


def test_do_train(
        universal:UniversalManager, 
        iterator:UniversalIterator, 
        epochs:int=10, 
        machine:dict[str,Any]={}, 
        model_continue_train:bool=False, 
        model_name:str="",
        now:str=""):

    # machine get id

    print("Try test do train")
    epoch_register = last_register(machine["id"])

    # machine load model
    if epoch_register and model_continue_train:
        pass

    # done

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    cpu = 'cpu'

    best_valid_loss = float('inf')

    universal.train()
    total = {
        Axis.E:0.,
        Axis.U:0.,
        Axis.N:0.
    }
    loss_metrics_train = LossAndMetricsManager.create()
    loss_metrics_valid = LossAndMetricsManager.create()
    # select axes to train
    axes = [Axis.E]

    for epoch in range(epochs):
        loss_metrics_train.new_metrics(epoch)
        loss_metrics_valid.new_metrics(epoch)

        start_time = time.time()
        # train part
        iterator.set_stage("train")
        train_iterator = iterator.get()
        train(
            train_iterator, 
            universal, 
            device, 
            epoch,
            loss_metrics_train, 
            softDTW, 
            axes)

        iterator.set_stage("validation")
        valid_iterator = iterator.get()
        evaluate(
            valid_iterator, 
            universal,
            device,
            epoch,
            loss_metrics_valid, 
            softDTW, 
            axes)
        end_time = time.time()

        for axis in axes:
            loss_metrics_train.set_status(axis)
            loss_metrics_valid.set_status(axis)
            train_loss, train_acc, train_dtw = loss_metrics_valid.elems()
            valid_loss, valid_acc, valid_dtw = loss_metrics_valid.elems()

            data_loss = {
                "train":{"loss":train_loss, "dtw":train_dtw},
                "valid":{"loss":valid_loss, "dtw":valid_dtw}
            }
            save_data_loss(model_name, epoch, data_loss, axis, now)
            # get the tail
            mean_valid_loss = np.mean([b for a,b in valid_loss[-600::]])
            mean_train_loss = np.mean([b for a,b in train_loss[-600::]])

            #If we find a smaller loss, we save the model
            if mean_valid_loss < best_valid_loss:
              best_valid_loss = mean_valid_loss
              universal.save(epoch, best_valid_loss, now, axis)
            # measure time
            epoch_mins, epoch_secs = epoch_time(start_time, end_time)

            logging.info(f'Axis {axis},\t Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
            logging.info(f'Axis {axis},\t Train Loss: {mean_train_loss:.3f}')
            logging.info(f'Axis {axis},\t Val. Loss: {mean_valid_loss:.3f}')
            

if __name__=="__main__":
    now = dt_filename()
 
    #nn.CrossEntropyLoss()# clasification
    print(torch.cuda.is_available())
    model_continue_train = False

    dataset_selection = {
        Stage.TRAIN: [0],
        Stage.VALIDATION: [1],
        Stage.TEST:[2]
    }
    model_name = "cnn_lineal_baseline_minitest"
    """
    Get log filename from machine already exists

    """
    filename = None
    if model_continue_train:
        machine_id=1
        machine = get_machine(machine_id)
        filename = machine.get("log_path")    

    log_filename = create_logging(
        this_directory, 
        model_name,
        logging.DEBUG, 
        filename)

    # define hiperparameters
    learning_rate = 0.001
    momentum = 0.9
    batch_size_train = 64
    batch_size_test = 64
    epochs = 1
    window = 320

    
    # create the manager for 3 axis
    models_path = this_directory.parent.parent / f"models/{model_name}/"
    models_path.mkdir(parents=True, exist_ok=True)
    logging.info(f"Models path {models_path}")

    channels=1
    model_manager = ModelManager.create(
        CNNLinealBaseline, 
        models_path,
        channels, 
        window=window)
    optimizer = OptimizerManager.create(
        model_manager, 
        learning_rate)
    criterion = CriterionManager.create()

    datasetpath, universal_iterator = load_dataset_iterators(
        this_directory.parent.parent / "datasets_test",
        dataset_selection, 
        batch_size_train,
        window)

    # move to cuda
    universal = UniversalManager(
        model_manager, 
        optimizer, 
        criterion)
    ### dataset iterators
    register_params = {
        "name":model_name,
        "description":"""This train model is created to check the
        trainning workflow""",
        "hyperparameters":{
            **model_manager.main_parameters(), 
            "learning_rate":learning_rate, 
            "selection":dataset_selection,
            "momentum":momentum, 
            "batch_size":{
                "train":batch_size_train, 
                "test":batch_size_test}},
        "optimizer":"OptimizerManager|Adam",
        "criterion":"CriterionManager|MSELoss",
        "epochs":epochs,
        "log_path":str(log_filename),
        "dataset_path":str(datasetpath.parent),
        "dataset_size":getFolderSize(str(datasetpath.parent)),
        "ml_path":str(models_path)
    }

    if not model_continue_train:
        machine = new_machine(register_params)
        print("Result register machine", machine["id"])
    ###########

    
    test_do_train(
        universal, 
        universal_iterator, 
        epochs, 
        machine, 
        model_continue_train, 
        model_name,
        now=now)
