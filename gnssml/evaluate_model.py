from gnssml.managers import (
    Axis,
    UniversalManager,
    LossAndMetricsManager)
from gnssml.tools import (
    dt_filename, 
    save_data_loss, 
    calculate_dtw, 
    epoch_time,
    get_memory)
import logging
import torch

def evaluate(
        iterator, 
        universal:UniversalManager,
        device:str,
        epoch:int,
        loss_metrics:LossAndMetricsManager, 
        softDTW, 
        axes):

    #We put the network in testing mode
    #In this mode, Pytorch doesn't use features only reserved for 
    #training (dropout for instance)    
    universal.eval()

    with torch.no_grad(): #disable the autograd engine (save
        #computation and memory)

      for selector in iterator:
        loss_metrics.new_metrics(epoch)
        for axis in axes:
            loss_metrics.set_status(axis)

            selector.set_status(axis)
            (x, y), distances, position = selector.values()
            #y = y[1,:].squeeze(dim=2)
            logging.info(f"Iteraring on axis {axis}")
            universal.set_status(axis)
            softDTW.set_status(axis)
            model, optimizer, criterion = universal.objects()
            model.to(device)
            model.set_distances(distances)
            model.set_position(position)
            x = x.cuda()
            y = y.cuda()
            y_pred = model(x)
            loss = criterion(y_pred, y)
            # accuracy not in regression
            dtw = calculate_dtw(softDTW,y_pred, y)
            loss_metrics.add_dtw(dtw.item())
            loss_metrics.add_loss(loss.item())


