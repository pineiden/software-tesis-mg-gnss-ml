from torch import nn
import logging
import torch.nn.functional as F
import torch

def normalize(A):
    A -= 0
    A /= 24*60*60
    return A

def dist_normalize(A, radio=100_000):
    return A/radio

class CNNLinealBaseline(nn.Module):
    def __init__(self,channels,*args,**kwargs):
        self.distances = []
        self.position = []
        dropout = kwargs.get("dropout",.15) 
        self.window = kwargs.get("window", 120)
        if "window" in kwargs:
            del kwargs["window"]
        super().__init__(*args,**kwargs)
        self.device = torch.device('cuda' if torch.cuda.is_available() else
                          'cpu')

        self.channels = channels

        self.model_parameters = {}
        self.model_parameters["order"] = ["conv1", "pool","conv2",
                                          "conv3", "relu", "fc1",
                                          "fc2"]
        self.cnn()
        self.lineal()
        #
        self.dropout = nn.Dropout(dropout)
        logging.info("CNN lineal baseline created")

    def set_distances(self, distances):
        self.distances = dist_normalize(distances)

    def set_position(self, position):
        self.position = position

    def cnn(self):
        out_channels = 8*self.channels
        params = {
            "in_channels": self.channels,
            "out_channels": out_channels,
            "kernel_size": (3,3),
        }
        self.conv1 = nn.Conv2d(**params)
        self.model_parameters["conv1"] = (self.channels, out_channels)
        in_channels = out_channels
        out_channels = 48*self.channels
        params = {
            "in_channels": in_channels,
            "out_channels": out_channels,
            "kernel_size": (1,1),
        }
        self.conv2 = nn.Conv2d(**params)
        self.model_parameters["conv2"] = (in_channels, out_channels)

        in_channels = out_channels
        out_channels = 164*self.channels
        params = {
            "in_channels": in_channels,
            "out_channels": out_channels,
            "kernel_size": (1,1),
        }

        self.conv3 = nn.Conv2d(**params)
        self.model_parameters["conv2"] = (in_channels, out_channels)

        logging.info("Created convolution layers")

    def lineal(self):
        # window size
        base_len = self.window
        print("BASE LEN", base_len)
        # output: 600 points
        len_group = 5
        len_pos = 3
        # the number of points output of conv stage is heavily
        # determinied by windows size.
        len_input = 26396 + len_group + len_pos
        self.fc1 = nn.Linear(len_input, 7200)
        self.model_parameters["fc1"] = (len_input, 7200)

        self.fc2 = nn.Linear(7200, base_len)
        self.model_parameters["fc2"] = (7200, base_len)

        logging.info(f"Created lineal layers: {self.fc1} - {self.fc2}")

    def forward(self, x_in):
        x_in = x_in.to(self.device)
        x_in = normalize(x_in)
        #timestamp vector
        x_ts  = x_in[:,:,:,0]
        x_ts = x_ts.view(x_ts.size(0), -1)

        #matriz measurements grouped

        x = x_in[:,:,:,1::]
        start = x
        # x es 600x5 -> batch: [N,1,600,5] 
        # breakpoint()
        x1  = self.conv1(x)
        x = F.relu(F.max_pool2d(x1,2))
        x = self.dropout(x)

        x2  = self.conv2(x)
        x3 = self.conv3(x2)
        x = self.dropout(x3)

        x = x.view(start.size(0), -1)
        #x  = torch.reshape(x, (1,-1))
        # concatenate this x featured with x_ts

        mat_distances = self.distances.repeat(x_ts.shape[0],1).to(self.device).float()
        mat_position = self.position.repeat(x_ts.shape[0],1).to(self.device).float()

        # here join with 'distances'
        x = torch.cat([x, x_ts, mat_distances, mat_position], axis=1)
        #breakpoint()
        # check shape before lineal layer
        x = F.relu(self.fc1(x))

        x = self.dropout(x)
        x = self.fc2(x)

        return x.squeeze()


    def main_parameters(self):
        return {
            **self.model_parameters,
        }
